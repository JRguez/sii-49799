#pragma once

#include "Raqueta.h"
#include "Esfera.h"

class DatosMemCompartida
{
	public:
		Esfera esfera;
		Raqueta raqueta1;
		int accion;//1 arriba, -1 abajo, 0 nada
};
