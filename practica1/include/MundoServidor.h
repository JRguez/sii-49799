// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/mman.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<pthread.h>
#include<Socket.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//DatosMemCompartida incluye a esfera y raqueta por lo que las puedo suprimir.
//#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
	
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	//logger
	int tuberia;
	//bot
	//DatosMemCompartida datos;
	//DatosMemCompartida *m;//puntero de la clase DatosMemCompartida
	//int mem;
	//Cliente
	//int tuberiaMC;
	//Servidor
	//int tuberiaMS;
	//int tubCoord;
	//int tubTec;
	pthread_t th1;

	Socket Soc_Conex;	
	Socket Soc_Comun;

	char nickname[200];

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
