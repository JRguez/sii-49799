// MundoServidor.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
	CMundo* p=(CMundo*) d;
	p -> RecibeComandosJugador();
}


CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(tuberia);
	//close(tubCoord);//practica3
	//close(tubTec);//practica3
	
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	


//SERVIDOR: LOGGER

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.0275f);
//bot
	//m->raqueta1 = jugador1;
	//m->esfera = esfera;
	
	//if(m->accion==1)OnKeyboardDown('w',0,0);
	//else if(m->accion==-1)OnKeyboardDown('s',0,0);
//fin bot


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		char cad[200];
		sprintf(cad,"Jugador 2 anoto un punto, lleva un total de %d puntos",puntos2);
		write(tuberia,cad,strlen(cad)+1);
		if(esfera.radio>0.05)
		{
			if(puntos2%2==0)
			{
			esfera.radio-=0.05;
			}
		if(puntos2==3)
		{
		printf("\nEl jugador 2 ha ganado la partida\n");
		printf("Jugador1 %d - %d Jugador2\n\n",puntos1,puntos2);
		exit(0);
		}
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		char cad[200];
		sprintf(cad,"Jugador 1 anoto un punto, lleva un total de %d puntos",puntos1);
		write(tuberia,cad,strlen(cad)+1);
		if(esfera.radio>0.05)
		{
			if(puntos1%2==0)
			{
			esfera.radio-=0.05;
			}
			if(puntos1==3)
			{
			printf("\nEl jugador 1 ha ganado la partida\n");
			printf("Jugador1 %d - %d Jugador2\n\n",puntos1,puntos2);
			exit(0);
			}
		}
	}
//Cadena de texto con los datos a enviar por parte del sevidor
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", 				esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
	//write(tubCoord,cad,sizeof(cad));//practica3
	Soc_Comun.Send(cad,sizeof(cad));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-3;break;
	case 'w':jugador1.velocidad.y=3;break;
	case 'l':jugador2.velocidad.y=-3;break;
	case 'o':jugador2.velocidad.y=3;break;

	}
}

void CMundo::Init()
{
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//logger
	tuberia=open("/tmp/TuberiaLogger",O_WRONLY);
//bot
	//Abro fichero
	//mem=open("/tmp/Filebot",O_CREAT|O_RDWR,0777);//creo el archivo y puedo leer y escribir.
	//Escribo
	//write(mem,&datos,sizeof(datos));
	//Proyecto en memoria	
	//m=(DatosMemCompartida*) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,mem,0);		
								    //el tipo de acceso lectura o escritura.
	//Cierro fichero
	//close(mem);
//Servidor
	//tubCoord=open("/tmp/tuberiaMC",O_WRONLY);//practica3
	//tubTec=open("/tmp/tubTecla",O_RDONLY);//practica3
	pthread_create(&th1,NULL,hilo_comandos,this);

//Socket

	Soc_Conex.InitServer("192.168.1.87",10000);
	Soc_Comun=Soc_Conex.Accept();
	Soc_Comun.Receive(nickname,sizeof(nickname));
	printf("Nombre:%c",nickname);


}

void CMundo::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char cad[100];
		Soc_Comun.Receive(cad, sizeof(cad));
		unsigned char key;
		sscanf(cad,"%c",&key);
		if (key=='s')jugador1.velocidad.y=-3;
		if (key=='w')jugador1.velocidad.y=3;
		if (key=='l')jugador2.velocidad.y=-3;
		if (key=='o')jugador2.velocidad.y=3;
	}
}













