#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/mman.h>
#include<unistd.h>

#include "DatosMemCompartida.h"

int main()
{
	DatosMemCompartida *mem;//variable de tipo puntero a la clase DatosMemCompartida
//Abro fichero
	int file=open("/tmp/Filebot",O_RDWR);
//Proyeccion en memoria	
	mem=(DatosMemCompartida*) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,file,0);
									//acceso lectura y escritura
//Cierro fichero
	close(file);

//Condiciones
while(1)
{
//Comparación de la parte inferior y superior de la raqueta con el centro de la pelota para ver su movimiento.	
	if(mem->raqueta1.y1 < mem->esfera.centro.y) mem->accion=1;
	else if(mem->raqueta1.y2 > mem->esfera.centro.y) mem->accion=-1;
	usleep(25000);
}
return 0;
}
